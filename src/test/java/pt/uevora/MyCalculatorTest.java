package pt.uevora;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class MyCalculatorTest {

    public MyCalculator calculator;

    @Before
    public void setUp(){
        calculator = new MyCalculator();
    }

    @After
    public void down(){
        calculator = null;
    }

    @Test
    public void testSum() throws Exception {
        final Double result = calculator.execute("3+2");

        assertEquals("The sum result of 3 + 2 must be 5",  5D, (Object)result);
    }
    
    @Test
    public void testMulti() throws Exception {
        final Double result = calculator.execute("3*2");
        
        assertEquals("The multiplication of 3*2 must be 6", 6D, (Object)result);
    }
    
    @Test
    public void testDivide() throws Exception {
        final Double result = calculator.execute("3/2");
        
        assertEquals("The division of 3/2 must be 3/2", 1.5D, (Object)result);
    }
    
    @Test
    public void testSubtract() throws Exception {
        final Double result = calculator.execute("2-3");
        
        assertEquals("The subtraction of 2-3 must be -1", -1D, (Object)result);
    }
    
    @Test
    public void testPotence() throws Exception {
    	final Double result = calculator.execute("2^3");
    	
    	assertEquals("The potence of 2 over 3 must be 8",8D,(Object)result);
    }

}